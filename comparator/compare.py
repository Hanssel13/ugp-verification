#!/usr/bin/python3

import sys
import os.path
import string

if len(sys.argv) < 3:
    sys.exit('usage: compare.py sim1 sim2')

stats = [[],[]]
for idx, fi in enumerate(sys.argv[1:3]):
    with open(fi) as fp:
        lines = fp.readlines()
        for line in lines: 
            if line.find("Z0:") == -1: continue
            # Example string:
            #Z0:      46102 [1] [2d2d2d2d2d2d|2d2d2d2d] pc=[00000744] W[r10=00000002][1] R[r26=00000002] R[r 0=00000000] [0|00000007] inst=[000d0513] DASM(000d0513)
            time = int(line[4:14].replace(' ',''))
            act = int(line[16:17].replace('x','0'))
            rdw = int(line[73:74].replace('x','0'))
            # The instruction is not active
            if not act: 
                # For some instructions, the rd is written after, so if we detect
                # this kind of behavior, we are just going to replace the rd
                # info of this wait state to the previous executed instruction
                if rdw and len(stats[idx]) != 0:
                    obj = {"pc": int(line[47:55].replace('x','0').replace('X','0'), 16), 
                           "act": act,
                           "rdi": int(line[60:62].replace(' ', '').replace('x','0').replace('X','0')),
                           "rdd": int(line[63:71].replace('x','0').replace('X','0'), 16),
                           "rdw": rdw,
                           "time": time
                          }
                    stats[idx][-1]["rdi"] = obj["rdi"];
                    stats[idx][-1]["rdd"] = obj["rdd"];
                    stats[idx][-1]["rdw"] = obj["rdw"];
                    stats[idx][-1]["time"] = obj["time"];
                continue # Instructions in wait state are not analyzed
            obj = {"pc": int(line[47:55].replace('x','0').replace('X','0'), 16), 
                   "act": act,
                   "rdi": int(line[60:62].replace(' ', '').replace('x','0').replace('X','0')),
                   "rdd": int(line[63:71].replace('x','0').replace('X','0'), 16),
                   "rdw": rdw,
                   "time": time
                  }
            stats[idx].append(obj);
            #print(obj)

#print(stats[0]);
#print ("\n");
#print(stats[1]);
# We are going to assume that the two executions starts at time
# But do not end at time
pc_errors = 0;
rd_errors = 0;
for idx, (a1, a2) in enumerate(zip(stats[0], stats[1])):
    # Compare pc
    if a1["pc"] != a2["pc"]:
        print("PC diff: (" + str(a1["time"]) + "," + str(a2["time"]) + ") 0x" + ("00000000" + hex(a1["pc"])[2:])[-8:] + ", " + ("00000000" + hex(a2["pc"])[2:])[-8:]);
    if a1["rdw"] != a2["rdw"]:
        print("RDE diff: (" + str(a1["time"]) + "," + str(a2["time"]) + ") " + str(a1["rdw"]) + ", " + str(a2["rdw"]));
    if a1["rdw"] and a2["rdw"] and a1["rdi"] != a2["rdi"]:
        print("RDI diff: (" + str(a1["time"]) + "," + str(a2["time"]) + ") " + str(a1["rdi"]) + ", " + str(a2["rdi"]));
    if a1["rdw"] and a2["rdw"] and a1["rdd"] != a2["rdd"] and a1["rdi"] == a2["rdi"] and a1["rdi"] != 0:
        print("RDD diff: (" + str(a1["time"]) + "," + str(a2["time"]) + ") " + str(a1["rdd"]) + ", " + str(a2["rdd"]));

