#!/usr/bin/python3

import sys
import os.path
import string

if len(sys.argv) < 3:
    sys.exit('usage: compare.py sim1 sim2')

stats = [[],[]]
for idx, fi in enumerate(sys.argv[1:3]):
    with open(fi) as fp:
        lines = fp.readlines()
        for line in lines: 
            if line.find("Z0:") == -1: continue
            # Example string:
            #Z0:      46102 [1] [2d2d2d2d2d2d|2d2d2d2d] pc=[00000744] W[r10=00000002][1] R[r26=00000002] R[r 0=00000000] [0|00000007] inst=[000d0513] DASM(000d0513)
            obj = {"pc": int(line[47:55], 16), 
                   "act": int(line[16:17]),
                   "rdi": int(line[60:62].replace(' ', '')),
                   "rdd": int(line[63:71], 16),
                   "rdw": int(line[74:75])
                  }
            stats[idx].append(obj);



