#!/usr/bin/python
import processor
import time
import sys
def loadhex(filepath):
	hex_vect=[]
	f=open(filepath,'r')
	for line in f:
		hex_vect.append(int(line,16))
	f.close()
	return hex_vect

if __name__ == "__main__":
	if(len(sys.argv)>2):	
		f1=open(sys.argv[2],'w')
		p1=processor.processor();
		hex_vect=loadhex(sys.argv[1])
		p1.program(hex_vect);
		p1.reset();
		report=p1.run()
		f1.write(report)
		f1.close()
	else:
		print "not enought input arguments"


