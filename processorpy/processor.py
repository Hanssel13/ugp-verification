import math

class processor:
	def __init__(self):
		self.reg = [0]*32;
		self.reg[0] = 0;
		self.memory = [0]*0x20000;
	  	self.compprog = []; # self has the instructions decoded, if memory changes, self isnt change!
		self.dac=0
		self.gpio = [];
		self.adc=0
		self.pc = 0x10000000;
		self.rdcycle = 0;
		self.rdcycleh = 0;
		self.rdtime = 0;
		self.rdtimeh = 0;
		self.rdinstret = 0;
		self.rdinstreth = 0;
		self.bPoints = [];
	  	self.stdout = "";
		self.err=0;
		self.writeback=0;
		self.hex=[];
		self.functions= {
			'lui':self.lui,
			'auipc':self.auipc,
			'jal':self.jal,
			'jalr':self.jalr,
			'beq':self.beq,
			'bne':self.bne,
			'blt':self.blt,
			'bge':self.bge,
			'bltu':self.bltu,
			'bgeu':self.bgeu,
			'lb':self.lb,
			'lbu':self.lbu,
			'lh':self.lh,
			'lhu':self.lhu,
			'lw':self.lw,
			'sb':self.sb,
			'sh':self.sh,
			'sw':self.sw,
			'addi':self.addi,
			'slti':self.slti,
			'sltiu':self.sltiu,
			'xori':self.xori,
			'ori':self.ori,
			'andi':self.andi,
			'slli':self.slli,
			'srli':self.srli,
			'srai':self.srai,
			'add':self.add,
			'sub':self.sub,
			'sll':self.sll,
			'srl':self.srl,
			'sra':self.sra,
			'slt':self.slt,
			'sltu':self.sltu,
			'xor':self.xor,
			'or_1':self.or_1,
			'and_1':self.and_1,
			'ebreak':self.ebreak,
			'illisn':self.illisn,
			'csrrw':self.csrrw,
			'csrrs':self.csrrs,
			'csrrc':self.csrrc,
			'csrrwi':self.csrrwi,
			'csrrsi':self.csrrsi,
			'csrrci':self.csrrci,
			'mul':self.mul,
			'mulh':self.mulh,
			'mulhu':self.mulhu,
			'mulhsu':self.mulhsu,
			'div':self.div,
			'divu':self.divu,
			'rem':self.rem,
			'remu':self.remu,
			'getq':self.getq,
			'setq':self.setq,
			'retirq':self.retirq,
			'maskirq':self.maskirq,
			'waitirq':self.waitirq,
			'timer':self.timer}

#-----------------------------------------------------------------------------------------------
# 	INSTRUCTION DECLARATION
	def lui(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = imm & 0xFFFFFFFF;
		self.pc = self.pc + 4;
		
	def auipc(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.pc + imm;
		self.pc = self.pc + 4;
		
	def jal(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.pc + 4;
		self.pc = self.pc + imm;

	def jalr(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.pc + 4;
		self.pc = self.reg[rs1] + imm;

	def beq(self,rs1, rs2, rd, imm):
		if(self.reg[rs1] == self.reg[rs2]):
			self.pc = self.pc + imm;
		else:
			self.pc = self.pc + 4;

	def bne(self,rs1, rs2, rd, imm):
		if(self.reg[rs1] != self.reg[rs2]):
			self.pc = self.pc + imm;
		else: 
			self.pc = self.pc + 4;

	def blt(self,rs1, rs2, rd, imm):
		if(self.reg[rs1] < self.reg[rs2]):
			self.pc = self.pc + imm;
		else:
			 self.pc = self.pc + 4;
	
	def bge(self,rs1, rs2, rd, imm):
		if(self.reg[rs1] >= self.reg[rs2]):
			self.pc = self.pc + imm;
		else:
			self.pc = self.pc + 4;
	
	def bltu(self,rs1, rs2, rd, imm):
		op1 = self.reg[rs1];
		op1 = self.rshift(op1,0);
		op2 = self.reg[rs2];
		op2 = self.rshift(op2, 0);
		if(op1 < op2):
			self.pc = self.pc + imm;
		else:
			self.pc = self.pc + 4;

	def bgeu(self,rs1, rs2, rd, imm):
		op1 = self.reg[rs1];
		op1 = self.rshift(op1, 0);
		op2 = self.reg[rs2];
		op2 = self.rshift(op2, 0);
		if(op1 >= op2):
			self.pc = self.pc + imm;
		else:
			self.pc = self.pc + 4;

	def lb(self,rs1, rs2, rd, imm):
		if(rd!=0): 
			addr = self.reg[rs1] + imm;
			tmp = addr&~(0x3);
			data = self.readMemory(tmp);
			ex= addr&0x3;
			data=data>>(ex*8)&0xff;
			data=self.comp2(data,8);
			self.reg[rd]=data;
		self.pc = self.pc + 4;
		self.writeback = 1;
	
	def lbu(self,rs1, rs2, rd, imm):
		if(rd!=0): 
			addr = self.reg[rs1] + imm;
			tmp = addr&~(0x3);
			data = self.readMemory(tmp);
			ex= addr&0x3;
			data=(data>>(ex*8))&0xff;
			self.reg[rd]=data;
		self.pc = self.pc + 4;
		self.writeback = 1;

	def lh(self,rs1, rs2, rd, imm):
		if(rd!=0):
			addr = self.reg[rs1] + imm;
			tmp = addr&~(0x2);
			data = self.readMemory(tmp);
			ex= (addr&0x2)>>1;
			data=data>>(ex*16)&0xffff;
			data=self.comp2(data,16);
			self.reg[rd]=data;
		self.pc = self.pc + 4;
		self.writeback = 1;

	def lhu(self,rs1, rs2, rd, imm):
		if(rd!=0):
			addr = self.reg[rs1] + imm;
			tmp = addr&~(0x2);
			data = self.readMemory(tmp);
			ex= (addr&0x2)>>1;
			data=data>>(ex*16)&0xffff;
			self.reg[rd]=data;
		self.pc = self.pc + 4;
		self.writeback = 1;

	def lw(self,rs1, rs2, rd, imm):
		if(rd!=0): 
			addr = self.reg[rs1] + imm;
			data = self.readMemory(addr);
			self.reg[rd]=data&0xFFFFFFFF;
		self.pc = self.pc + 4;
		self.writeback = 1;
	
	def sb(self,rs1, rs2, rd, imm):
		addr=self.reg[rs1]+imm;
		ex = addr&0x3;
		strb= 1<<ex;
		data = self.reg[rs2]&0xff;
		data=data<<24|data<<16|data<<8|data;
		addr=addr&~(0x3);
		self.writeMemory(addr,data,strb);
		self.pc = self.pc + 4;	

	def sh(self,rs1, rs2, rd, imm):
		addr=self.reg[rs1]+imm;
		ex = (addr&0x2)>>1;
		strb= 0x3<<(ex*2);
		data = self.reg[rs2]&0xffff;
		data=data<<16|data;
		addr=addr&~(0x2);
		self.writeMemory(addr,data,strb);
		self.pc = self.pc + 4;	

	def sw(self,rs1, rs2, rd, imm):
		addr=self.reg[rs1]+imm;
		strb= 0xf;
		data = self.reg[rs2]&0xffffffff;
		self.writeMemory(addr,data,strb);
		self.pc = self.pc + 4;

	def addi(self,rs1, rs2, rd, imm):
		op1 = self.reg[rs1];
		op1 = self.rshift(op1,0);
		op2 = imm;
		op2 = self.rshift(op2, 0);
		if(rd != 0):
			res = (op1 + op2) & 0xFFFFFFFF;
			self.writeback = 1
			self.reg[rd] = res;
		self.pc = self.pc + 4;

	def slti(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = 1 if (self.reg[rs1] < imm) else 0;
		self.pc = self.pc + 4;
	

	def sltiu(self,rs1, rs2, rd, imm):
		if(rd != 0):
			op1 = self.reg[rs1];
			op1 = self.rshift(op1, 0);
			op2 = imm;
			op2 = self.rshift(op2, 0);
			self.writeback = 1
			self.reg[rd] = 1 if(op1 < op2) else 0;
		self.pc = self.pc + 4;

	def xori(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.reg[rs1] ^ (imm & 0xFFFFFFFF);
		self.pc = self.pc + 4;

	def ori(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.reg[rs1] | (imm & 0xFFFFFFFF);
		self.pc = self.pc + 4;

	def andi(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.reg[rs1] & (imm & 0xFFFFFFFF);
		self.pc = self.pc + 4;

	def slli(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = int((self.reg[rs1] << rs2) & 0xFFFFFFFF);	# ?
		self.pc = self.pc + 4;

	def srli(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.rshift(self.reg[rs1],rs2) & 0xFFFFFFFF;
		self.pc = self.pc + 4;

	def srai(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.reg[rs1] >> rs2;
		self.pc = self.pc + 4;

	def add(self,rs1, rs2, rd, imm):
		op1 = self.reg[rs1];
		op1 = self.rshift(op1,0);
		op2 = self.reg[rs2];
		op2 = self.rshift(op2, 0);
		if(rd != 0):
			res = (op1 + op2) & 0xFFFFFFFF;
			self.writeback = 1
			self.reg[rd] = res;
		self.pc = self.pc + 4;

	def sub(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = (self.reg[rs1] - self.reg[rs2]) & 0xFFFFFFFF;
		self.pc = self.pc + 4;

	def sll(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = int((self.reg[rs1] << (self.reg[rs2] & 0xFFFFFFFF)) & 0xFFFFFFFF);	# ?
		self.pc = self.pc + 4;

	def srl(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.rshift(self.reg[rs1],self.reg[rs2]) & 0xFFFFFFFF;
		self.pc = self.pc + 4;

	def sra(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.reg[rs1] >> (self.reg[rs2] & 0xFFFFFFFF);
		self.pc = self.pc + 4;

	def slt(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = 1 if (self.reg[rs1] < self.reg[rs2]) else 0;
		self.pc = self.pc + 4;

	def sltu(self,rs1, rs2, rd, imm):
		if(rd != 0):
			op1 = self.reg[rs1];
			op1 = self.rshift(op1, 0);
			op2 = self.reg[rs2];
			op2 = self.rshift(op2, 0);
			self.writeback = 1
			self.reg[rd] =1 if (op1 < op2) else 0;
		self.pc = self.pc + 4;
	

	def xor(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.reg[rs1] ^ self.reg[rs2];
		self.pc = self.pc + 4;
	

	def or_1(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.reg[rs1] | self.reg[rs2];
		self.pc = self.pc + 4;
	
	def and_1(self,rs1, rs2, rd, imm):
		self.writeback = 1
		if(rd != 0):
			self.reg[rd] = self.reg[rs1] & self.reg[rs2];
		self.pc = self.pc + 4;
	
	def ebreak(self,rs1, rs2, rd, imm):
		self.bPoints.append(self.pc);
		#print("EBREAK activated");
	
  	def illisn(self,rs1, rs2, rd, imm):
    		print("ILLEGAL INSTRUCTION");
	def csrrw(self,rs1, rs2, rd, imm):
		# self function does nothing
		self.pc = self.pc + 4;
	

	def csrrs(self,rs1, rs2, rd, imm):
		if(rd != 0):
			#rdcycle
			if(imm == 0xC000): rd = self.rdcycle; 
			if(imm == 0xC800): rd = self.rdcycleh; 
			#rdtime
			if(imm == 0xC001): rd = self.rdtime; 
			if(imm == 0xC801): rd = self.rdtimeh; 
			#rdinstret
			if(imm == 0xC002): rd = self.rdinstret; 
			if(imm == 0xC802): rd = self.rdinstreth;
		self.pc = self.pc + 4;
	

	def csrrc(self,rs1, rs2, rd, imm):
		# self function does nothing
		self.pc = self.pc + 4;
	

	def csrrwi(self,rs1, rs2, rd, imm):
		# self function does nothing
		self.pc = self.pc + 4;
	

	def csrrsi(self,rs1, rs2, rd, imm):
		# self function does nothing
		self.pc = self.pc + 4;
	

	def csrrci(self,rs1, rs2, rd, imm):
		# self function does nothing
		self.pc = self.pc + 4;
	 

	def mul(self,rs1, rs2, rd, imm):
		if(rd != 0):	
			op1 = self.reg[rs1];
			op2 = self.reg[rs2];
			res = (op1 * op2) & 0xFFFFFFFF;
			self.writeback = 1
			self.reg[rd] = res;
		self.pc = self.pc + 4;
	

	def mulh(self,rs1, rs2, rd, imm):	
		if(rd != 0):
			op1 = self.reg[rs1] #int(str(self.reg[rs1]),16); 
			op2 = self.reg[rs2] #int(str(self.reg[rs2]),16);
			#op1 = op1>>>0; op2 = op2>>>0;
			#op1 = op1.toString(10); ##Preguntar a hanssel
			#op2 = op2.toString(10);
			rel = math.floor((op1 * op2) / 4294967296);
			self.writeback = 1
			self.reg[rd] = int(rel) & 0xFFFFFFFF;
		self.pc = self.pc + 4;

	def mulhu(self,rs1, rs2, rd, imm):	
		if(rd != 0):
			op1 = self.reg[rs1]; 
			op2 = self.reg[rs2];
			op1 = self.rshift(op1,0);
			op2 = self.rshift(op2,0);
			#op1 = op1.toString(10);
			#op2 = op2.toString(10);
			rel = math.floor((op1 * op2) / 4294967296);
			self.writeback = 1
			self.reg[rd] = int(rel) & 0xFFFFFFFF;
		self.pc = self.pc + 4;
	

	def mulhsu(self,rs1, rs2, rd, imm):	
		if(rd != 0):
			op1 = self.reg[rs1]; 
			op2 = self.reg[rs2];
			op2 = self.rshift(op2,0);
			#op1 = op1.toString(10);
			#op2 = op2.toString(10);
			rel = math.floor((op1 * op2) / 4294967296);
			self.writeback = 1
			self.reg[rd] = int(rel) & 0xFFFFFFFF;
		self.pc = self.pc + 4;
		
	def div(self,rs1, rs2, rd, imm):	
		if(rd != 0):
			op1 = self.reg[rs1]; 
			op2 = self.reg[rs2];
			if (op2 == 0):
			  rel = -1;
			else:
			  rel = op1 / op2;
			self.writeback = 1
			self.reg[rd] = int(rel) & 0xFFFFFFFF;
		self.pc = self.pc + 4;
		
	def divu(self,rs1, rs2, rd, imm):	
		if(rd != 0):
			op1 = self.reg[rs1]; 
			op2 = self.reg[rs2];
			op1 = self.rshift(op1,0);
			op2 = self.rshift(op2,0);
			if(op2 == 0):
			  rel = 0xFFFFFFFF;
			else:
			  rel = op1 / op2;
			self.writeback = 1
			self.reg[rd] = int(rel) & 0xFFFFFFFF;
		self.pc = self.pc + 4;
		
	def rem(self,rs1, rs2, rd, imm):	
		if(rd != 0):
			op1 = self.reg[rs1]; 
			op2 = self.reg[rs2];
			if(op2 == 0):
			  rel = op1;
			else:
			  rel = op1 % op2;
			self.writeback = 1
			self.reg[rd] = int(rel) & 0xFFFFFFFF;
		self.pc = self.pc + 4;
		
	def remu(self,rs1, rs2, rd, imm):	
		if(rd != 0):
			op1 = self.reg[rs1]; 
			op2 = self.reg[rs2];
			op1 = self.rshift(op1,0);
			op2 = self.rshift(op2,0);
			if(op2 == 0):
			  rel = op1;
			else:
			  rel = op1 % op2;
			self.writeback = 1
			self.reg[rd] = int(rel) & 0xFFFFFFFF;
		self.pc = self.pc + 4;

	def getq(self,rs1, rs2, rd, imm):
		self.pc = self.pc + 4;
	

	def setq(self,rs1, rs2, rd, imm):
		self.pc = self.pc + 4;
	

	def retirq(self,rs1, rs2, rd, imm):
		self.pc = self.pc + 4;
	

	def maskirq(self,rs1, rs2, rd, imm):
		self.pc = self.pc + 4;
	

	def waitirq(self,rs1, rs2, rd, imm):
		self.pc = self.pc + 4;
	

	def timer(self,rs1, rs2, rd, imm):
		self.pc = self.pc + 4;
	
#------------------------------------------------------------------------------------------------
#FUNCTIONS FOR DEBUG
	def run(self):
		strout=""
		err="ERROR0 RUN"		
		while(1):
			stop = False;
			for bPi in range(0,len(self.bPoints)): #; bPi++):
				if(self.bPoints[bPi] == self.pc):
					print("Execution stopped at pc="+hex(self.pc))#.toString(16));
					stop = True;
					break;
				
			
			if(stop): break;
			#self.simulate(1);
			#try:
			strout+=self.simulate(1);
				#self.
			#except Exception as e:
			#	print(e);
			#	return 0; #OJO hanssel
		return strout;
			
		
	
	

	def insertBreak(self,bPoints):			
		self.bPoints = self.bPoints.append(bPoints);
	

	def deleteBreak(self,index):
		if(index >= len(self.bPoints)): #.length): ## TODO
			self.bPoints.splice(index,1);
	

	def getBreakNum(self):
		return len(self.bPoints); #.length;
	
#----------------------------------------------------------------------------------------------
#	FUNCTION FOR RESET REG
	def reset(self):
		for ii in range(1,32):# = 1; ii < 32; ii++): ## hanssel
			self.reg[ii]=0;
		self.pc=0x10000000;
		self.rdinstret=0;	
		self.rdcycle = 0;
		self.rdcycleh = 0;
		self.rdtime = 0;
		self.rdtimeh = 0;
		self.rdinstreth = 0;
		self.bPi=0;
		self.bPoints=[];
    	#self.stdout = "";
	
	def format_r(self,num):
		if num>9:
			str_rd="r"+str(num)
		else:
			str_rd="r "+str(num)
		return str_rd 
#-----------------------------------------------------------------------------------------------
#	ENTRY POINT FOR SIMULATE
	def simulate(self,ninstr):
		for li in range(0,ninstr): # = 0; li < ninstr; li++): 
			# Fetch instruction
			#print(self.pc);
			prog = self.compprog[(self.pc - 0x10000000) >> 2];
			# Execute instruction (possible ebreak)
			#print(prog["state"])
			pc = self.pc
			self.functions[prog["state"]](prog["rs1"], prog["rs2"], prog["rd"], prog["imm"]);
			if prog["rd"] == None: 
				prog["rd"] = 0;
			if prog["rs1"] == None: prog["rs1"] = 0;
			if prog["rs2"] == None: prog["rs2"] = 0;
			s = "Z0:"+("          "+str(self.rdinstret))[-11:]+" [1] [2d2d2d2d2d2d|2d2d2d2d] pc=["+("00000000"+hex(pc)[2:])[-8:]+"] W["+self.format_r(prog["rd"])+"="+("00000000"+hex(self.reg[prog["rd"]])[2:])[-8:]+"]["+str(self.writeback)+"] R["+self.format_r(prog["rs1"])+"="+("00000000"+hex(self.reg[prog["rs1"]])[2:])[-8:]+"] R["+self.format_r(prog["rs2"])+"="+("00000000"+hex(self.reg[prog["rs2"]])[2:])[-8:]+"] [0|00000007] inst=["+("00000000"+hex(self.hex[(pc-0x10000000)/4])[2:])[-8:]+"] DASM("+("00000000"+hex(self.hex[(pc-0x10000000)/4])[2:])[-8:]+")\n"
			# Increment
			if(self.rdcycle == 0xFFFFFFFC): 
				self.rdcycleh+=4;
				self.rdcycle = 0;
			else:
				self.rdcycle += 4;
			if(self.rdinstret == 0xFFFFFFFF): 
				self.rdinstreth+=1;
				self.rdinstret = 0;
			else:
				self.rdinstret+=1;
			# Increment
			self.rdtime = self.rdcycle;
			self.rdtimeh = self.rdcycleh;
			self.writeback = 0;
			return s
			#print(prog["state"]+" "+str(prog["rs1"])+" "+str(prog["rs2"])+" "+str(prog["rd"])+" "+str(prog["imm"]))
			
	

#-----------------------------------------------------------------------------------------------
#	FUNCTION FOR INTRODUCING PROGRAM MEMORY
	def program(self,data):
		for i in range(0,len(data)): # = 0; i < data.length; i++):
			if(type(data[i]) == int): #hanssel
				self.memory.append(data[i]);
				self.hex.append(data[i]);
				try:
					self.compprog.append(self.interpreter(data[i]));
				except:
					#print("illisn")
					self.compprog.append({"rs1":0, "rs2":0, "rd":0, "imm":0, "state":"illisn"})
				
			  
					
	
	
#-----------------------------------------------------------------------------------------------
#	FUNCTION FOR WATCHING VARIABLES
	def watch_reg(self):#ERR
		str_1 = "";
		for i in range(0,32):#; i < 32; i++)#ERR
			disp = "NODATA";
			if(self.reg[i] != None):
				# self shows negative numbers
				#uns = self.reg[i];
				# self shows the unsigned version
				uns = self.rshift(self.reg[i],0);
				disp = hex(uns) #.toString(16);
			
			str_1 += "x" + str(i) + " = 0x" + disp + "\n";#ERR
		
		str_1 += "\npc = 0x" + hex(self.pc) + "\n";#ERR
		str_1 += "instr = " + self.rdinstret + "\n";
		return str_1;
	
	
#-----------------------------------------------------------------------------------------------
#	FUNCTION FOR WATCHING MEMORY
	def watch_memory(self,base, size):
		str_1 = " Memory base: 0x" + hex(base) + " size: 0x" + hex(size) #.toString(16);
		true_base = base & ~(0xF);
		size_in_lines = base - true_base + size;
		size_in_lines = (size_in_lines >> 4) + (1 if(size_in_lines & 0xF) else 0);
		for i in range(0,size_in_lines): # = 0; i < size_in_lines; i++)
			tmp = (i<<4);
			tmp =self.rshift(temp,0); #tmp >>> 0;
			tmp = hex(tmp) #.toString(16);
			tmp = "\n" + "0x" +("00000000" + tmp)[-8:] + ": "; #Hanssel
			str_1 += tmp;
			for m in range(0,16): #(m = 0; m < 16; m++)
				addr = m + (i << 4);
				if((addr < base) or (addr >= (base + size))):
					str_1 += "   ";
				else:
					data = self.readMemory(addr & ~(0x3));
					data = (data >> (m*8)) & 0xFF;
					str_1 += ( "00" + hex(data))[-2:] + " ";
			#str += tmp + ": 0x" +  ( "00000000" + .toString(16)).slice(-8);
		return str_1;
	

#-----------------------------------------------------------------------------------------------
#	FUNCTION FOR WATCHING MEMORY
	def get_stdout(self):
		return self.stdout;
#-----------------------------------------------------------------------------------------------
	def writeMemory (self,addr,data,strb):
		if(addr%4!=0):
			print(str(addr) + " is not a is not a possible space of memory");
			return 1		
		
		if(addr==0x20000000):
			self.stdout += chr(data & 0xFF);
		elif(addr==0x30000000 and data==123456789):
			self.bPoints.append(self.pc);
		else:
			addr = addr - 0x10000000;
			tmp = self.memory[addr/4];
			for i in range(0,4): #(i=0;i<4;i++):
				if((strb>>i)&0x1):
					tmp=(tmp&~(0xff<<(i*8)))|(data&(0xff<<(i*8)));
			self.memory[addr/4]=tmp;
			return 0;

	def readMemory (self,addr):
		if(addr%4!=0):
			print(str(addr) + "is not a is not a possible space of memory");	
			return 1;
		if(addr==0x20000000):
			return 0xfff;	
		else:
			addr = addr - 0x10000000;
			return self.memory[addr/4];
#-------------------------------------------------------------------------------------------------------------	
	def interpreter (self,instruc):
		opcode=instruc&0x7f;
		funct3=(instruc>>12)&0x7;
		funct7=(instruc>>25)&0x7F;
		rd=(instruc>>7)&0x1f;
		rs1=(instruc>>15)&0x1f;
		rs2=(instruc>>20)&0x1f;
		imm=0;
		
		if opcode == 0x37:
			imm=(instruc & 0xfffff000);		
			state="lui";
			
		elif opcode == 0x17:
			imm=(instruc & 0xfffff000);
			state="auipc";
			
		elif opcode == 0x6f:
			imm=((instruc >>31)&0x1) << 20 |((instruc>>12)&0xff) << 12 | ((instruc>>20)&0x1) << 11 | ((instruc>>21)&0x3ff)<<1;
			imm=self.comp2(imm,21);			
			state="jal";
			
		elif opcode == 0x67:
			imm=(instruc>>20)&0xfff;
			imm=self.comp2(imm,12);		
			state="jalr";
			
		elif opcode == 0x63:
			imm=((instruc >> 31)&0x1) << 12 |((instruc>>7)&0x1) << 11 | ((instruc >> 25)&0x3f) << 5 |((instruc>>8)&0xf) << 1;
			imm=self.comp2(imm,13);			
			if funct3 == 0x0:
				state="beq";	
			elif funct3 == 0x1:
				state="bne";
			elif funct3 == 0x4:		
				state="blt";
			elif funct3 == 0x5:		
				state="bge";
			elif funct3 == 0x6:		
				state="bltu";
			elif funct3 == 0x7:		
				state="bgeu";
			else:
				print("invalid funct3");
		
		elif opcode == 0x3:
			imm=(instruc>>20)&0xfff;
			imm=self.comp2(imm,12);	
			if funct3 == 0x0:			
				state="lb";
			elif funct3 == 0x1:
				state="lh";
			elif funct3 == 0x2:			
				state="lw";
			elif funct3 == 0x4:			
				state="lbu";
			elif funct3 == 0x5:			
				state="lhu"; 
			else:
				print("invalid funct3");
				
		elif opcode == 0x23:
			imm=((instruc>>25)&0x7f)<<5|((instruc>>7)&0x1f);
			imm=self.comp2(imm,12);
			if funct3 == 0x0:			
				state="sb";
			elif funct3 == 0x1:			
				state="sh";
			elif funct3 == 0x2:			
				state="sw";
			else:
				print("invalid funct3");
				
		elif opcode == 0x13:
			imm=(instruc>>20)&0xfff;
			imm=self.comp2(imm,12);
			if funct3 == 0x0:			
				state="addi";	
			elif funct3 == 0x2:			
				state="slti";
			elif funct3 == 0x3:			
				state="sltiu";
			elif funct3 == 0x4:			
				state="xori";
			elif funct3 == 0x6:			
				state="ori";
			elif funct3 == 0x7:			
				state="andi";
			elif funct3 == 0x1:
				imm=(instruc>>20)&0x1f;
				imm=self.comp2(imm,5);		
				state="slli";
			elif funct3 == 0x5:
				imm=(instruc>>20)&0x1f;
				imm=self.comp2(imm,5);		
				if funct7 == 0x0:
					state="srli";
				elif funct7 == 0x20:
					state="srai";
				else:
					print("invalid funct7");
			else:
				print("invalid funct3");
				
		elif opcode == 0x33:	
			if funct3 == 0x0:
				if funct7 == 0x0:
					state="add";
					
				elif funct7 == 0x1:					
					state="mul";
					
				elif funct7 == 0x20:
					state="sub";
					
				else:
					raise  "invalid funct7";

			elif funct3 == 0x1:
				if funct7 == 0x0:
					state="sll";
					
				elif funct7 == 0x1:
					state="mulh";
					
				else:
					raise  "invalid funct7";

			elif funct3 == 0x2:
				if funct7 == 0x0:
					state="slt";
					
				elif funct7 == 0x1:
					state="mulhsu";
					
				else:
					raise  "invalid funct7";
					
			elif funct3 == 0x3:
				if funct7 == 0x0:
					state="sltu";
					
				elif funct7 == 0x1:
					state="mulhu";
					
				else:
					raise  "invalid funct7";
					
			elif funct3 == 0x4:	
				if funct7 == 0x0:		
					state="xor";
					
				elif funct7 == 0x1:
					state="div";
					
				else:
					raise  "invalid funct7";
					
			elif funct3 == 0x5:
				if funct7 == 0x0:
					state="srl";
					
				elif funct7 == 0x1:
					state="divu";
					
				elif funct7 == 0x20:
					state="sra";
					
				else:
					raise  "invalid funct7";

			elif funct3 == 0x6:
				if funct7 == 0x0:
					state="or_1";
					
				elif funct7 == 0x1:
					state="rem";
					
				else:
					raise  "invalid funct7";
				
			elif funct3 == 0x7:
				if funct7 == 0x0:
					state="and_1";
					
				elif funct7 == 0x1:
					state="remu";
					
				else:
					raise  "invalid funct7";
					
			else:
				raise  "invalid funct3";
				
		elif opcode == 0x73:
			imm=(instruc>>20)&0xfff;
			if funct3 == 0x0:
				state="ebreak";
			elif funct3 == 0x1:
				state="csrrw";
				
			elif funct3 == 0x2:
				state="csrrs";
				
			elif funct3 == 0x3:
				state="csrrc";
				
			elif funct3 == 0x5:
				state="csrrwi";
				
			elif funct3 == 0x6:
				state="csrrsi";
				
			elif funct3 == 0x7:
				state="csrrci";
			else:
				raise ("invalid funct3");		
		else:
			raise ("invalid opcode");
		return {"rs1":rs1, "rs2":rs2, "rd":rd, "imm":imm, "state":state}
	
	
	def comp2 (self,num,nbits):
		if((num >> (nbits-1)) & 0x1):
			num = ~num & ((1 << nbits)-1);
			num = (num + 1) & ((1 << nbits)-1);
			num = -num;
		return num;
		
	def rshift(self,val, n): 
		if n < 0: print n
		return (val % 0x100000000) >> (n & 0xFFFFFFFF)

