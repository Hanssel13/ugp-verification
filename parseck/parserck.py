#!/usr/bin/python
import sys

# Helper function for formatting rs registers
def format_r(num):
	if num>9:
		str_rd="r"+str(num)
	else:
		str_rd="r "+str(num)
	return str_rd

if __name__ == "__main__":
	
	# Make sure that
	if(len(sys.argv)>2):
		# Files
		f=open(sys.argv[1],'r')
		f2=open(sys.argv[2],'w')
		
		# Auxiliar vars
		strout=''
		count=0
		rs1="0"
		rs2="0"
		reg_rs1="0x00000000"
		reg_rs2="0x00000000"
		# Flag for capture instruction
		do_capture = False
		for line in f:
			
			# Separate line by spaces, then explore it
			linv=line.split(' ')
			
			# Remove empty separations
			while '' in linv:
				linv.remove('')
			
			# Lines that begins with 'core'
			# Format:
			# 0      1  2                  3            4,5,6,7...
			# core   0: 0x000000001000075c (0x800a0a13) addi    s4, s4, -2048
			# Format (in exception):
			# 0      1  2         3                         4   5
			# core   0: exception trap_illegal_instruction, epc 0x00000000100007dc
			# 0      1  2         3         4   5   6
			# core   0: exception interrupt #3, epc 0x0000000010000814
			# Format (tval, ignored):
			# 0      1            2    3
			# core   0:           tval 0x0000000000000000
			if(linv[0]=='core'):
				# Exception lines
				# Print a line for exception here
				if(linv[2] == 'exception'):
					cause = 7 # This is the default for olinguito, but can be any value
					# This is for detecting interruptions
					if(linv[3] == 'interrupt'):
						cause = (int(linv[4][1:-1]) | 1 << 31) & 0xFFFFFFFF
					# The rest of causes
					elif(linv[3] == 'trap_instruction_address_misaligned,'):
						cause = 0 # CAUSE_MISALIGNED_FETCH
					elif(linv[3] == 'trap_instruction_access_fault,'):
						cause = 1 # CAUSE_FETCH_ACCESS
					elif(linv[3] == 'trap_illegal_instruction,'):
						cause = 2 # CAUSE_ILLEGAL_INSTRUCTION
					elif(linv[3] == 'trap_breakpoint,'):
						cause = 3 # CAUSE_BREAKPOINT
					elif(linv[3] == 'trap_load_address_misaligned,'):
						cause = 4 # CAUSE_MISALIGNED_LOAD
					elif(linv[3] == 'trap_store_address_misaligned,'):
						cause = 5 # CAUSE_MISALIGNED_STORE
					elif(linv[3] == 'trap_load_access_fault,'):
						cause = 6 # CAUSE_LOAD_ACCESS
					elif(linv[3] == 'trap_store_access_fault,'):
						cause = 7 # CAUSE_STORE_ACCESS
					elif(linv[3] == 'trap_user_ecall,'):
						cause = 8 # CAUSE_USER_ECALL
					elif(linv[3] == 'trap_supervisor_ecall,'):
						cause = 9 # CAUSE_SUPERVISOR_ECALL
					elif(linv[3] == 'trap_hypervisor_ecall,'):
						cause = 10 # CAUSE_HYPERVISOR_ECALL
					elif(linv[3] == 'trap_machine_ecall,'):
						cause = 11 # CAUSE_MACHINE_ECALL
					elif(linv[3] == 'trap_instruction_page_fault,'):
						cause = 12 # CAUSE_FETCH_PAGE_FAULT
					elif(linv[3] == 'trap_load_page_fault,'):
						cause = 13 # CAUSE_LOAD_PAGE_FAULT
					elif(linv[3] == 'trap_store_page_fault,'):
						cause = 15 # CAUSE_STORE_PAGE_FAULT
					strout+="Z0:"+("          "+str(count))[-11:]+" [0] [2d2d2d2d2d2d|2d2d2d58] pc=["+pc[2:]+"] W[r 0=00000000][0] R[r 0=00000000] R[r 0=00000000] [1|" + ("00000000"+hex(cause)[2:])[-8:] + "] inst=["+("00000000"+dasm[2:])[-8:]+"] DASM("+("00000000"+dasm[2:])[-8:]+")\n"
					count+=1
				# Tval lines (ignored)
				elif(linv[2] == 'tval'):
					pass # Ignored
				# Regular core lines
				else:
					pc=linv[2][8:] # Save the pc (But only the last 8 numbers)
					dasm=linv[3][1:-1] # Save the dasm (machine code, ommiting () )
			
			# Lines that begins with 'RD'
			# Format (with writeback):
			# 0   1 2          3            4   5
			# RD  3 0x10000740 (0x003cdb13) x22 0x00000000
			# 0   1 2          3            4 5 6
			# RD  3 0x10000740 (0x003cdb13) x 2 0x00000000
			# Format (without writeback):
			# 0   1 2          3
			# RD  3 0x1000076c (0xff2bdee3)
			if(linv[0]=='RD'):
				pc=linv[2] # Save the pc
				dasm=linv[3][1:-1] # Save the dasm (machine code)
				# If the PC is valid on our memory map, then capture it (this ommits some dead code like the initial ROM)
				if int(pc, 16) >= 0x10000000: do_capture = True
				# If the last argument was a ')', means that this RD has indeed a writeback
				if(dasm[-1]==')'):
					writeback=0
					dasm=dasm[:-1] # This is for ommiting the ')' in the dasm
				else:
					writeback=1
					if(linv[4]=='x'): # The 'x' alone is present, 6 arguments
						rd=linv[5]
						reg_rd=linv[6][:-1]
					else:							# The 'x' is not present alone, 5 arguments
						rd=linv[4][1:]
						reg_rd=linv[5][:-1]
			
			# Lines that begins with 'RS1'
			# Format (with register usage):
			# 0   1 2          3            4   5
			# RS1 3 0x10000754 (0x00090c67) x18 0x10000748
			# 0   1 2          3            4 5 6
			# RS1 3 0x10000758 (0x80010a13) x 2 0x1001ffe8
			# Format (without register usage):
			# 0   1 2          3
			# RS1 3 0x10000758 (0x80010a13)
			elif(linv[0]=='RS1'):
				if(len(linv)>4):
					if(linv[4]=='x'):
						rs1=linv[5]
						reg_rs1=linv[6][:-1]
					else:
						rs1=linv[4][1:]
						reg_rs1=linv[5][:-1]
				else:
					pass # no RS1
			
			# Lines that begins with 'RS2'
			# Format (with register usage):
			# 0   1 2          3            4   5
			# RS1 3 0x10000754 (0x00090c67) x18 0x10000748
			# 0   1 2          3            4 5 6
			# RS1 3 0x10000758 (0x80010a13) x 2 0x1001ffe8
			# Format (without register usage):
			# 0   1 2          3
			# RS1 3 0x10000758 (0x80010a13)
			elif(linv[0]=='RS2'):
				if(len(linv)>4):
					if(linv[4]=='x'):
						rs2=linv[5]
						reg_rs2=linv[6][:-1]
					else:
						rs2=linv[4][1:]
						reg_rs2=linv[5][:-1]
				else:
					pass#no RS2
				
				# If present the RS2, and also is a capture, please show the transformed instruction
				if do_capture:
					strout+="Z0:"+("          "+str(count))[-11:]+" [1] [2d2d2d2d2d2d|2d2d2d2d] pc=["+pc[2:]+"] W["+format_r(int(rd))+"="+("00000000"+reg_rd[2:])[-8:]+"]["+str(writeback)+"] R["+format_r(int(rs1))+"="+("00000000"+reg_rs1[2:])[-8:]+"] R["+format_r(int(rs2))+"="+("00000000"+reg_rs2[2:])[-8:]+"] [0|00000007] inst=["+("00000000"+dasm[2:])[-8:]+"] DASM("+("00000000"+dasm[2:])[-8:]+")\n"
					rs1="0"
					rs2="0"
					reg_rs1="0x00000000"
					reg_rs2="0x00000000"
					do_capture = False
					count+=1
		f2.write(strout)
		f.close()
		f2.close()
	# Show usage if not enough arguments
	else:
		print "usage: ./parseck.py [dump_from_spike] [output]"
