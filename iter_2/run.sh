#!/bin/bash
[[ ! "$BASE_DIR" ]] && BASE_DIR=..
echo "Running the previous simulation..."
[[ -f start.o ]] || riscv32-unknown-elf-gcc -c $BASE_DIR/firmware/start.S -I$BASE_DIR/firmware/ -o start.o
[[ -f htif.o ]] || riscv32-unknown-elf-gcc -c $BASE_DIR/spike/htif.c -I$BASE_DIR/firmware/ -o htif.o
riscv32-unknown-elf-gcc -Os -march=rv32im -ffreestanding -nostdlib -o tprog -Wl,-Bstatic,-T,$BASE_DIR/firmware/sections.lds,-Map,tprog.map,--strip-debug start.o htif.o $BASE_DIR/iter_0/BEST_pop1.s -lgcc 2>error.log
riscv32-unknown-elf-objdump -d tprog > tprog.dump
riscv32-unknown-elf-objcopy -O binary tprog tprog.bin
python3 $BASE_DIR/firmware/makehex.py tprog.bin 16384 > tprog.hex
cp tprog.hex $BASE_DIR/../Olinguito/run_cadence/firmware.hex
rm -rf $BASE_DIR/../Olinguito/run_cadence/cov_work/
make -C $BASE_DIR/../Olinguito/run_cadence/ all
cp -r $BASE_DIR/../Olinguito/run_cadence/cov_work cov_work
echo "Finished running the simulation. Running ugp3"
./ugp3

