
  .text
  .global evolved_function
  .type evolved_function, @function
  evolved_function:
.LFB0:
  .cfi_startproc
  addi sp, sp, -8;
  sw ra, 4(sp);
  sw s0, 0(sp);
  addi s0, sp, 8;
        
                addi a5, sp, -2048;
                addi a5, a5, -2048;
                sh s9, (1011 << 1)(a5);
               
                jal zero, unique_unique_nMMK;
                unique_unique_nMMM:
                jal zero, unique_unique_nMML;
                unique_unique_nMMK:
                la s5, unique_unique_nMMM;
                jalr a2, s5, 0;
                unique_unique_nMML:
              
                addi s5, sp, -2048;
                addi s5, s5, -2048;
                sw a6, (-306 << 2)(s5);
              
                addi t0, sp, -2048;
                addi t0, t0, -2048;
                sb s11, -101(t0);
              
                addi s2, sp, -2048;
                addi s2, s2, -2048;
                lw t2, (16 << 2)(s2);
              
                addi a0, sp, -2048;
                addi a0, a0, -2048;
                sw t2, (475 << 2)(a0);
               
                jal zero, unique_unique_nMMN;
                unique_unique_nMMP:
                jal zero, unique_unique_nMMO;
                unique_unique_nMMN:
                la s8, unique_unique_nMMP;
                jalr s4, s8, 0;
                unique_unique_nMMO:
              
                addi t3, sp, -2048;
                addi t3, t3, -2048;
                lh t0, (-757 << 1)(t3);
               
                jal s3, label_nTXK;
               
                jal zero, unique_unique_nMMQ;
                unique_unique_nMMS:
                jal zero, unique_unique_nMMR;
                unique_unique_nMMQ:
                bltu s7, a1, unique_unique_nMMS;
                unique_unique_nMMR:
               
                jal zero, unique_unique_nMMT;
                unique_unique_nMMV:
                jal zero, unique_unique_nMMU;
                unique_unique_nMMT:
                la t5, unique_unique_nMMV;
                jalr tp, t5, 0;
                unique_unique_nMMU:
               
                bgeu a6, s2, label_nTXS;
               
                blt s9, t3, label_nTWQ;
              
                addi a4, sp, -2048;
                addi a4, a4, -2048;
                lw a7, (-220 << 2)(a4);
              label_nTWQ:  
                auipc a6, 0xf68c9;
              
                addi s5, sp, -2048;
                addi s5, s5, -2048;
                lhu a0, (873 << 1)(s5);
               
                jal zero, unique_unique_nMMW;
                unique_unique_nMMY:
                jal zero, unique_unique_nMMX;
                unique_unique_nMMW:
                bltu a7, t5, unique_unique_nMMY;
                unique_unique_nMMX:
               
                jal zero, unique_unique_nMMZ;
                unique_unique_nMM3:
                jal zero, unique_unique_nMM2;
                unique_unique_nMMZ:
                bltu t3, s7, unique_unique_nMM3;
                unique_unique_nMM2:
               
                bge a2, s6, label_nTXI;
               
                jal s10, label_nTXR;
               
                slti s1, s2, 1873;
               
                jal zero, unique_unique_nMM4;
                unique_unique_nMM6:
                jal zero, unique_unique_nMM5;
                unique_unique_nMM4:
                bgeu t6, s1, unique_unique_nMM6;
                unique_unique_nMM5:
              
                la t1, label_nTXG;
                jalr s11, t1, 0;
              
                addi t1, sp, -2048;
                addi t1, t1, -2048;
                sb a4, -1221(t1);
               
                jal zero, unique_unique_nMM7;
                unique_unique_nMNB:
                jal zero, unique_unique_nMNA;
                unique_unique_nMM7:
                bne t0, s8, unique_unique_nMNB;
                unique_unique_nMNA:
              label_nTW2:  
                jal s5, label_nTW2;
               
                jal zero, unique_unique_nMNC;
                unique_unique_nMNE:
                jal zero, unique_unique_nMND;
                unique_unique_nMNC:
                bne tp, s9, unique_unique_nMNE;
                unique_unique_nMND:
               
                jal zero, unique_unique_nMNF;
                unique_unique_nMNH:
                jal zero, unique_unique_nMNG;
                unique_unique_nMNF:
                jal s11, unique_unique_nMNH;
                unique_unique_nMNG:
               
                jal s9, label_nTXG;
               
                jal s7, label_nTXE;
               
                bne a5, a7, label_nTXW;
               
                beq a5, s7, label_nTXR;
               
                jal zero, unique_unique_nMNI;
                unique_unique_nMNK:
                jal zero, unique_unique_nMNJ;
                unique_unique_nMNI:
                bne a2, a3, unique_unique_nMNK;
                unique_unique_nMNJ:
               
                bge s10, s4, label_nTXD;
              label_nTXD:  
                jal zero, unique_unique_nMNL;
                unique_unique_nMNN:
                jal zero, unique_unique_nMNM;
                unique_unique_nMNL:
                jal t1, unique_unique_nMNN;
                unique_unique_nMNM:
              label_nTXE: 
                la s2, label_nTXK;
                jalr t1, s2, 0;
               
                slti t1, t2, 2;
              label_nTXG:  
                jal a0, label_nTXW;
              
                addi s0, sp, -2048;
                addi s0, s0, -2048;
                sh t1, (430 << 1)(s0);
              label_nTXI:  
                and s1, a0, t3;
              
                addi a2, sp, -2048;
                addi a2, a2, -2048;
                lbu t5, -1425(a2);
              label_nTXK:  
                sltiu tp, a3, 341;
               
                jal zero, unique_unique_nMN5;
                unique_unique_nMN7:
                jal zero, unique_unique_nMN6;
                unique_unique_nMN5:
                la a0, unique_unique_nMN7;
                jalr s2, a0, 0;
                unique_unique_nMN6:
               
                jal a0, label_nTXM;
              label_nTXM:  
                and t6, tp, s4;
               
                srli s6, s9, 3;
               
                jal zero, unique_unique_nMNO;
                unique_unique_nMNQ:
                jal zero, unique_unique_nMNP;
                unique_unique_nMNO:
                la s2, unique_unique_nMNQ;
                jalr s8, s2, 0;
                unique_unique_nMNP:
              
                addi s4, sp, -2048;
                addi s4, s4, -2048;
                lb s11, 511(s4);
               
                jal zero, unique_unique_nMNR;
                unique_unique_nMNT:
                jal zero, unique_unique_nMNS;
                unique_unique_nMNR:
                bge s7, s2, unique_unique_nMNT;
                unique_unique_nMNS:
              label_nTXR: 
                addi a0, sp, -2048;
                addi a0, a0, -2048;
                lhu s11, (-404 << 1)(a0);
              label_nTXS:  
                xori s3, s2, -1557;
               
                jal zero, unique_unique_nMNU;
                unique_unique_nMNW:
                jal zero, unique_unique_nMNV;
                unique_unique_nMNU:
                la s8, unique_unique_nMNW;
                jalr s9, s8, 0;
                unique_unique_nMNV:
              
                addi s0, sp, -2048;
                addi s0, s0, -2048;
                lb a1, 1564(s0);
               
                jal s5, label_nTXY;
              label_nTXW:  
                jal zero, unique_unique_nMNX;
                unique_unique_nMNZ:
                jal zero, unique_unique_nMNY;
                unique_unique_nMNX:
                beq s8, a5, unique_unique_nMNZ;
                unique_unique_nMNY:
              label_nTXX:  
                bne s1, s2, label_nTXX;
              label_nTXY:  
                jal zero, unique_unique_nMN2;
                unique_unique_nMN4:
                jal zero, unique_unique_nMN3;
                unique_unique_nMN2:
                bltu a1, s4, unique_unique_nMN4;
                unique_unique_nMN3:
                
                div a0, a1, a2
                divu a3, a4, a5
                rem s1, s2, s3
                remu s4, s5, s6
  
  lw ra, 4(sp);
  lw s0, 0(sp);
  addi sp, sp, 8;
  jalr zero, ra, 0;
  .cfi_endproc
.LFE0:
  .size evolved_function, .-evolved_function
  .ident "MicroGP"
  .section .note.GNU-stack,"",@progbits
        
