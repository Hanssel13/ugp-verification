#!/bin/bash

rm -fv core
rm -fv fitness.out
rm -fv i*.s
rm -fv *.log
rm -fv main.o
rm -fv statistics.*
rm -fv status.xml
rm -fv tprog
rm -fv individualsToEvaluate.txt
rm -fv *.tmp
rm -fv *.txt
rm -fv *.o
rm -fv *.bin
rm -fv *.hex
rm -fv *.elf
rm -fv statistics*
rm -fv status*
rm -rfv html
rm -fv *.log
rm -fv tprog
rm -fv iccr.key
rm -fv *.dump
rm -fv *.swp
rm -fv *.out
rm -fv *.pyc
rm -fv *.map
