#!/bin/bash
# Starting from v3.1.2_1142 fitness scripts can use (again) environment variables:
# $UGP3_FITNESS_FILE : the file created by the evaluator
# $UGP3_OFFSPRING    : the individuals to be evaluated (space separated list)
# $UGP3_GENERATION   : generation number
# $UGP3_VERSION      : current ugp3 version. eg. 3.1.2_1142
# $UGP3_TAGLINE      : full ugp3 tagline. eg. ugp3 (MicroGP++) v3.1.2_1142 "Bluebell"
RUN_PROCESSORPY=0
[[ ! "$BASE_DIR" ]] && BASE_DIR=..
[[ ! "$UGP3_FITNESS_FILE" ]] && UGP3_FITNESS_FILE=fitness.out

# Clean up directory
rm -f tprog error.log $UGP3_FITNESS_FILE tprog.out tprog.hex

# Compile
BASE_DIR=$BASE_DIR ./compile.sh $1

# Get tprog output & number of lines

# Simulate using our processor
#echo "Olinguito lauched"
cp tprog.hex $BASE_DIR/../Olinguito/run_cadence/firmware.hex
rm -rf $BASE_DIR/../Olinguito/run_cadence/cov_work/
make -C $BASE_DIR/../Olinguito/run_cadence/ all &> olinguito_exec_out.tmp
#echo "Olinguito finish"

# Simulate using spike
#echo "spike launched"
timeout 5s spike -l -m0x10000000:0x20000,0x30000000:0x1000 tprog < /dev/null &> spike_exec.tmp
spike_killd=$?

if [[ $spike_killd == 0 ]]; then
  python $BASE_DIR/parseck/parserck.py spike_exec.tmp spike_exec_out.tmp
  if [ $? != 0 ]; then cp $1 errorp.s; fi
elif [[ $spike_killd != 124 ]]; then
  cp $1 errorps.s
  exit 1
fi

# Simulate using processorpy
#echo "processorpy launched"
processorpy_killd=0
if [[ $RUN_PROCESSORPY != 0 ]]; then
  timeout 5s python $BASE_DIR/processorpy/execute.py tprog.hex processorpy_exec_out.tmp > /dev/null
  processorpy_killd=$?
fi

if [[ $processorpy_killd != 0 ]]; then
  if [[ $processorpy_killd != 124 ]]; then
    cp $1 errorpp.s
    exit 2
  fi
fi

# Do comparisons
if [[ $processorpy_killd == 0 ]] && [[ $spike_killd == 0 ]]; then
  python3 $BASE_DIR/comparator/compare.py olinguito_exec_out.tmp spike_exec_out.tmp > olinguito_spike.tmp
  if [ $? != 0 ]; then cp $1 error.s; fi
  if [[ $RUN_PROCESSORPY != 0 ]]; then
    python3 $BASE_DIR/comparator/compare.py olinguito_exec_out.tmp processorpy_exec_out.tmp > olinguito_processorpy.tmp
    if [ $? != 0 ]; then cp $1 error.s; fi
  fi
  len_spike=$(wc -l olinguito_spike.tmp | awk '{ print $1 }')
  if [[ $RUN_PROCESSORPY != 0 ]]; then
    len_processorpy=$(wc -l olinguito_processorpy.tmp | awk '{ print $1 }')
  else
    len_processorpy=0
  fi
  if [[ $len_spike != "0" ]] || [[ $len_processorpy != "0" ]]; then
    mkdir diffs/$1
    cat olinguito_exec_out.tmp | spike-dasm &> diffs/$1/olinguito_exec_out.tmp
    cat spike_exec_out.tmp | spike-dasm &> diffs/$1/spike_exec_out.tmp
    cp spike_exec.tmp diffs/$1
    cp olinguito_spike.tmp diffs/$1
    cp $1 diffs/$1
    cp tprog.dump diffs/$1
    cp tprog diffs/$1
    cp tprog.hex diffs/$1
    if [[ $RUN_PROCESSORPY != 0 ]]; then
      cat processorpy_exec_out.tmp | spike-dasm &> diffs/$1/processorpy_exec_out.tmp
      cp olinguito_processorpy.tmp diffs/$1
      python3 $BASE_DIR/comparator/compare.py spike_exec_out.tmp processorpy_exec_out.tmp > diffs/$1/spike_processorpy.tmp
    fi
  fi
fi

rm -rf html
iccr -test $BASE_DIR/../Olinguito/run_cadence/cov_work/scope/test/ ./iccr_extract_html.cf &> html.log
python parse.py temp.txt
# TODO: Analyze if there is a timeout
# TODO: Extract the coverage and put into a
a=$(cat temp.txt)

if [[ a == 0 ]]; then
  cp $1 infloop.s
fi

len=$(wc -l $1 | awk '{ print $1 }')
(( b = 10000 - $len )); [[ $b < 0 ]] && b=0 # (paranoia)
note="$1/$len"

echo "$a $note" >$UGP3_FITNESS_FILE

