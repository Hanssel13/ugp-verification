#!/bin/bash
[[ ! "$BASE_DIR" ]] && BASE_DIR=..

# Check if some files are already compiled
[[ -f start.o ]] || riscv32-unknown-elf-gcc -c $BASE_DIR/firmware/start.S -I$BASE_DIR/firmware/ -o start.o
[[ -f entry.o ]] || riscv32-unknown-elf-gcc -c $BASE_DIR/firmware/entry.S -I$BASE_DIR/firmware/ -o entry.o
[[ -f htif.o ]] || riscv32-unknown-elf-gcc -c $BASE_DIR/spike/htif.c -I$BASE_DIR/firmware/ -o htif.o

# Compile & execute
riscv32-unknown-elf-gcc -Os -march=rv32im -ffreestanding -nostdlib -o tprog -Wl,-Bstatic,-T,$BASE_DIR/firmware/sections.lds,-Map,tprog.map,--strip-debug start.o entry.o htif.o $1 -lgcc 2>error.log
riscv32-unknown-elf-objdump -d tprog > tprog.dump
riscv32-unknown-elf-objcopy -O binary tprog tprog.bin
python3 $BASE_DIR/firmware/makehex.py tprog.bin 16384 > tprog.hex
#riscv32-unknown-elf-gcc -o tprog main.o $1 -lm -lc 2>error.log
if [[ ! -f tprog.hex ]]; then
    echo " "
    echo " "
    echo "PANIK! Can't compile the test program (tprog.hex)"
    echo " "
    cat error.log
    exit
fi

