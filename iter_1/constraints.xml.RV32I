<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="http://www.cad.polito.it/ugp3/transforms/constraintsScripted.xslt"?>
<constraints
    xmlns="http://www.cad.polito.it/ugp3/schemas/constraints" 
    id="One-Max" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    schemaLocation="http://www.cad.polito.it/ugp3/schemas/constraints http://www.cad.polito.it/ugp3/schemas/constraints.xsd">
  <typeDefinitions>
    <item type="constant" name="register">
      <!-- ra, sp, gp ignored as they are used for functions -->
      <value>tp</value>
      <value>t0</value>
      <value>t1</value>
      <value>t2</value>
      <value>s0</value>
      <value>s1</value>
      <value>a0</value>
      <value>a1</value>
      <value>a2</value>
      <value>a3</value>
      <value>a4</value>
      <value>a5</value>
      <value>a6</value>
      <value>a7</value>
      <value>s2</value>
      <value>s3</value>
      <value>s4</value>
      <value>s5</value>
      <value>s6</value>
      <value>s7</value>
      <value>s8</value>
      <value>s9</value>
      <value>s10</value>
      <value>s11</value>
      <value>t3</value>
      <value>t4</value>
      <value>t5</value>
      <value>t6</value>
    </item>

    <item type="constant" name="instruction_reg">
      <value>add</value>
      <value>sub</value>
      <value>and</value>
      <value>or</value>
      <value>xor</value>
      <value>slt</value>
      <value>sltu</value>
      <value>sll</value>
      <value>srl</value>
      <value>sra</value>
    </item>

    <item type="constant" name="instruction_imm">
      <value>addi</value>
      <value>andi</value>
      <value>ori</value>
      <value>xori</value>
      <value>slti</value>
      <value>sltiu</value>
    </item>

    <item type="constant" name="instruction_mem_byte">
      <value>sb</value>
      <value>lb</value>
      <value>lbu</value>
    </item>

    <item type="constant" name="instruction_mem_half">
      <value>sh</value>
      <value>lh</value>
      <value>lhu</value>
    </item>

    <item type="constant" name="instruction_mem_word">
      <value>lw</value>
      <value>sw</value>
    </item>

    <item type="constant" name="instruction_imm_shift">
      <value>slli</value>
      <value>srli</value>
      <value>srai</value>
    </item>

    <item type="constant" name="instruction_imm_U">
      <value>lui</value>
      <value>auipc</value>
    </item>
    
    <item type="constant" name="branch">
      <value>bne</value>
      <value>beq</value>
      <value>blt</value>
      <value>bge</value>
      <value>bltu</value>
      <value>bgeu</value>
    </item>

    <item type="constant" name="instruction_mul_div_rem">
      <value>mul</value>
      <value>mulh</value>
      <value>mulhu</value>
      <value>mulhsu</value>
      <value>div</value>
      <value>divu</value>
      <value>rem</value>
      <value>remu</value>
    </item>

  </typeDefinitions>
  <commentFormat><value/></commentFormat>
  <identifierFormat>n<value /></identifierFormat>
  <labelFormat>label_<value/>: </labelFormat>
  <uniqueTagFormat>unique_<value /></uniqueTagFormat>
  <prologue id="globalPrologue"/>
  <epilogue id="globalEpilogue"/>
  <sections>
    <section id="bitString" prologueEpilogueCompulsory="false">
      <prologue id="sectionPrologue">
        <expression>
  .text
  .global evolved_function_2
  .type evolved_function_2, @function
  evolved_function_2:
.LFB0:
  .cfi_startproc
  addi sp, sp, -8;
  sw ra, 4(sp);
  sw s0, 0(sp);
  addi s0, sp, 8;
        </expression>
      </prologue>
      <epilogue id="sectionEpilogue">
        <expression>
  lw ra, 4(sp);
  lw s0, 0(sp);
  addi sp, sp, 8;
  jalr zero, ra, 0;
  .cfi_endproc
.LFE0:
  .size evolved_function_2, .-evolved_function_2
  .ident "MicroGP"
  .section .note.GNU-stack,"",@progbits
        </expression>
      </epilogue>
      <subSections>
        <subSection id="main" maxOccurs="1" minOccurs="1" maxReferences="0">
          <prologue id="stringPrologue"/>
          <epilogue id="stringEpilogue"/>
          
          <macros maxOccurs="1000" minOccurs="1" averageOccurs="70" sigma="60">
            <!-- Instruction performing operations (op rd, rs1, rs2) -->
            <macro id="instruction_reg_insert">
              <expression> 
                <param ref="ins"/> <param ref="rd"/>, <param ref="rs1"/>, <param ref="rs2"/>;
              </expression>
              <parameters>
                <item type="definedType" ref="instruction_reg" name="ins" />
                <item type="definedType" ref="register" name="rd" />
                <item type="definedType" ref="register" name="rs1" />
                <item type="definedType" ref="register" name="rs2" />
              </parameters>
            </macro>

            <!-- Instruction performing operations (op rd, rs1, imm) -->
            <macro id="instruction_imm_insert">
              <expression> 
                <param ref="ins"/> <param ref="rd"/>, <param ref="rs1"/>, <param ref="imm"/>;
              </expression>
              <parameters>
                <item type="definedType" ref="instruction_imm" name="ins" />
                <item type="definedType" ref="register" name="rd" />
                <item type="definedType" ref="register" name="rs1" />
                <item type="integer" minimum="-2048" maximum="2047" name="imm" />
              </parameters>
            </macro>

            <!-- Instruction performing shifting with imm (sXX rd, rs1, imm) -->
            <macro id="instruction_imm_shift_insert">
              <expression> 
                <param ref="ins"/> <param ref="rd"/>, <param ref="rs1"/>, <param ref="imm"/>;
              </expression>
              <parameters>
                <item type="definedType" ref="instruction_imm_shift" name="ins" />
                <item type="definedType" ref="register" name="rd" />
                <item type="definedType" ref="register" name="rs1" />
                <item type="integer" minimum="0" maximum="31" name="imm" />
              </parameters>
            </macro>
        
            <!-- Instruction storing to register from imm (lui/auipc rd, imm) -->
            <macro id="instruction_imm_U_insert">
              <expression> 
                <param ref="ins"/> <param ref="rd"/>, 0x<param ref="imm"/>;
              </expression>
              <parameters>
                <item type="definedType" ref="instruction_imm_U" name="ins" />
                <item type="bitArray" length="20" base="hex" name="imm" />
                <item type="definedType" ref="register" name="rd" />
              </parameters>
            </macro>
            
            <!-- Instruction performing branches (forward only, bXX rs1, rs2, label) -->
            <macro id="branchCondForward">
              <expression> 
                <param ref="ins"/> <param ref="rs1"/>, <param ref="rs2"/>, label_<param ref="label"/>;
              </expression>
              <parameters>
                <item type="definedType" ref="branch" name="ins" />
                <item type="definedType" ref="register" name="rs1" />
                <item type="definedType" ref="register" name="rs2" />
                <item type="innerForwardLabel" name="label" itself="true" prologue="false" epilogue="false"/>
              </parameters>
            </macro>
            
            <!-- Instruction performing branches (backward only, bXX rs1, rs2, label) -->
            <macro id="branchCondBackward">
              <expression> 
                jal zero, unique_<param ref="tag1"/>;
                unique_<param ref="tag3"/>:
                jal zero, unique_<param ref="tag2"/>;
                unique_<param ref="tag1"/>:
                <param ref="ins"/> <param ref="rs1"/>, <param ref="rs2"/>, unique_<param ref="tag3"/>;
                unique_<param ref="tag2"/>:
              </expression>
              <parameters>
                <item name="tag1" type="uniqueTag" />
                <item name="tag2" type="uniqueTag" />
                <item name="tag3" type="uniqueTag" />
                <item type="definedType" ref="branch" name="ins" />
                <item type="definedType" ref="register" name="rs1" />
                <item type="definedType" ref="register" name="rs2" />
              </parameters>
            </macro>
            
            <!-- Instruction performing inconditional jumps (forward only, jal rd, label) -->
            <macro id="jalCondForward">
              <expression> 
                jal <param ref="rd"/>, label_<param ref="label"/>;
              </expression>
              <parameters>
                <item type="definedType" ref="register" name="rd" />
                <item type="innerForwardLabel" name="label" itself="true" prologue="false" epilogue="false"/>
              </parameters>
            </macro>
            
            <!-- Instruction performing branches (backward only, jal rd, label) -->
            <macro id="jalCondBackward">
              <expression> 
                jal zero, unique_<param ref="tag1"/>;
                unique_<param ref="tag3"/>:
                jal zero, unique_<param ref="tag2"/>;
                unique_<param ref="tag1"/>:
                jal <param ref="rd"/>, unique_<param ref="tag3"/>;
                unique_<param ref="tag2"/>:
              </expression>
              <parameters>
                <item name="tag1" type="uniqueTag" />
                <item name="tag2" type="uniqueTag" />
                <item name="tag3" type="uniqueTag" />
                <item type="definedType" ref="register" name="rd" />
              </parameters>
            </macro>
            
            <!-- Instruction performing inconditional jumps, register-based (forward only, jalr rd, rs1, label) -->
            <macro id="jalrCondForward">
              <expression>
                la <param ref="rs1"/>, label_<param ref="label"/>;
                jalr <param ref="rd"/>, <param ref="rs1"/>, 0;
              </expression>
              <parameters>
                <item type="definedType" ref="register" name="rd" />
                <item type="definedType" ref="register" name="rs1" />
                <item type="innerForwardLabel" name="label" itself="true" prologue="false" epilogue="false"/>
              </parameters>
            </macro>
            
            <!-- Instruction performing branches (backward only, jalr rd, rs1, label) -->
            <macro id="jalrCondBackward">
              <expression> 
                jal zero, unique_<param ref="tag1"/>;
                unique_<param ref="tag3"/>:
                jal zero, unique_<param ref="tag2"/>;
                unique_<param ref="tag1"/>:
                la <param ref="rs1"/>, unique_<param ref="tag3"/>;
                jalr <param ref="rd"/>, <param ref="rs1"/>, 0;
                unique_<param ref="tag2"/>:
              </expression>
              <parameters>
                <item name="tag1" type="uniqueTag" />
                <item name="tag2" type="uniqueTag" />
                <item name="tag3" type="uniqueTag" />
                <item type="definedType" ref="register" name="rd" />
                <item type="definedType" ref="register" name="rs1" />
              </parameters>
            </macro>
            
            <!-- Instruction saving/loading bytes (lb/lbu/sb rs2/rd, imm(rs1)) -->
            <!-- The desired address to save/load must be in the stack -->
            <macro id="instruction_mem_byte_insert">
              <expression>
                addi <param ref="rs1"/>, sp, -2048;
                addi <param ref="rs1"/>, <param ref="rs1"/>, -2048;
                <param ref="ins"/> <param ref="rs2"/>, <param ref="imm"/>(<param ref="rs1"/>);
              </expression>
              <parameters>
                <item type="definedType" ref="instruction_mem_byte" name="ins" />
                <item type="definedType" ref="register" name="rs1" />
                <item type="definedType" ref="register" name="rs2" />
                <item type="integer" minimum="-2048" maximum="2047" name="imm" />
              </parameters>
            </macro>
            
            <!-- Instruction saving/loading half words (lh/lhu/sh rs2/rd, imm(rs1)) -->
            <!-- The desired address to save/load must be in the stack -->
            <macro id="instruction_mem_half_insert">
              <expression>
                addi <param ref="rs1"/>, sp, -2048;
                addi <param ref="rs1"/>, <param ref="rs1"/>, -2048;
                <param ref="ins"/> <param ref="rs2"/>, (<param ref="imm"/> &lt;&lt; 1)(<param ref="rs1"/>);
              </expression>
              <parameters>
                <item type="definedType" ref="instruction_mem_half" name="ins" />
                <item type="definedType" ref="register" name="rs1" />
                <item type="definedType" ref="register" name="rs2" />
                <item type="integer" minimum="-1024" maximum="1023" name="imm" />
              </parameters>
            </macro>
            
            <!-- Instruction saving/loading words (lw/sw rs2/rd, imm(rs1)) -->
            <!-- The desired address to save/load must be in the stack -->
            <macro id="instruction_mem_word_insert">
              <expression>
                addi <param ref="rs1"/>, sp, -2048;
                addi <param ref="rs1"/>, <param ref="rs1"/>, -2048;
                <param ref="ins"/> <param ref="rs2"/>, (<param ref="imm"/> &lt;&lt; 2)(<param ref="rs1"/>);
              </expression>
              <parameters>
                <item type="definedType" ref="instruction_mem_word" name="ins" />
                <item type="definedType" ref="register" name="rs1" />
                <item type="definedType" ref="register" name="rs2" />
                <item type="integer" minimum="-512" maximum="511" name="imm" />
              </parameters>
            </macro>
            
          </macros>
        </subSection>
      </subSections>   
    </section>
  </sections>
</constraints>
